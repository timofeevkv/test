from flask import Flask, request, render_template
from model import get_model, predict
import numpy as np

app = Flask(__name__)

def get_prediction(params):
    prediction = predict(params)
    return str(prediction)


@app.route('/predict_form/', methods=['post', 'get'])
def login():
    message = ''
    if request.method == 'POST':
        params = request.form.get('username')
        params_list = params.split(" ")
        params = [float(param) for param in params_list]
        params = np.array([params])

        message = get_prediction(params)

    return render_template('login.html', message=message)


if __name__ == '__main__':
    app.run()
